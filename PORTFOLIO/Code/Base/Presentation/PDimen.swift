//
//  PDimen.swift
//  PORTFOLIO
//
//  Created by Andres Felipe Alzate on 16/06/2020.
//  Copyright © 2020 aalzres. All rights reserved.
//

import UIKit

struct PDimen {
    static let paddingXXXS: CGFloat = 2
    static let paddingXXS: CGFloat = 4
    static let paddingXS: CGFloat = 8
    static let paddingS: CGFloat = 16
    static let paddingM: CGFloat = 24
    static let paddingL: CGFloat = 32
    static let paddingXL: CGFloat = 64
    static let paddingXXL: CGFloat = 128
    static let paddingXXXL: CGFloat = 256
        
    static let animationDuration = 0.2 // Default all over iOS
    
    static let underlineWidth: CGFloat = 1.0
    
    static let visibleAlpha: CGFloat = 1.0
    static let disabledAlpha: CGFloat = 0.5
    static let translucentAlpha: CGFloat = 0.3
    static let invisibleAlpha: CGFloat = 0.0
    
    static let componentButtonRadius: CGFloat = componentButtonHeight/2
    static let componentButtonHeight: CGFloat = 56
    static let componentButtonBorderWidth: CGFloat = 2
    static let componentButtonWidthMultiplier: CGFloat = 0.9
    
    static let inputHeight: CGFloat = 50
    static let inputTitleTextSize: CGFloat = 16
    static let inputTextSize: CGFloat = 18
    static let inputButtonWidth: CGFloat = 30
    static let inputButtonHeight: CGFloat = inputButtonWidth
    
    static let cornerRadius: CGFloat = 15
    static let shadowRadius: CGFloat = 2
    
    static let title: CGFloat = 28
    static let subtitle: CGFloat = 18
    static let primary: CGFloat = 16
    static let secondary: CGFloat = 14
}
